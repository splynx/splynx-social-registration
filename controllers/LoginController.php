<?php

namespace app\controllers;

use app\models\Customers;
use app\models\SplynxCustomer;
use app\models\SplynxCustomers;
use app\models\SplynxLead;
use splynx\helpers\ConfigHelper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

class LoginController extends Controller
{
    /**
     * @return array<mixed>
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    /**
     * This function will be triggered when user is successfully authenticated using some oAuth client.
     *
     * @param \yii\authclient\ClientInterface $client
     * @return void
     */
    public function onAuthSuccess($client)
    {
        switch ($client->getId()) {
            case 'google':
                $this->userHandler($client, Customers::GOOGLE_PREFIX, 'emails.0.value');
                break;
            case 'twitter':
                $this->userHandler($client, Customers::TWITTER_PREFIX, 'email');
                break;
            case 'facebook':
                $this->userHandler($client, Customers::FACEBOOK_PREFIX, 'email');
                break;
        }
    }

    /**
     * @param string $socialId
     * @return SplynxCustomer|SplynxLead|null
     */
    private function getIdentity($socialId)
    {
        return (SplynxCustomers::get())->getCustomerBySocialId($socialId);
    }

    /**
     * @param \yii\authclient\ClientInterface $client
     * @param string $prefix
     * @param string $email_format
     * @return Response
     * @throws \Exception
     */
    protected function userHandler($client, $prefix, $email_format)
    {
        $attributes = $client->getUserAttributes();
        $identity = $this->getIdentity($prefix . $attributes['id']);

        if (!$identity) {
            $social_email = ArrayHelper::getValue($attributes, $email_format);
            /** @var SplynxCustomer|SplynxLead|null $customer */
            $customer = (SplynxCustomers::get())->getRealCustomerByAttribute('email', $social_email);
            if (
                $customer && isset($customer->email) && SplynxCustomer::assertValueExists(
                    $customer->email,
                    $social_email
                )
            ) {
                if ($customer->setSocialIdForCustomer($prefix . $attributes['id'])) {
                    Yii::$app->session->set('splynx_customer_id', $customer['id']); // @phpstan-ignore-line
                    return $this->redirect(ConfigHelper::get('splynx_url') . 'portal');
                }
            }
        } else {
            Yii::$app->session->set('splynx_customer_id', $identity['id']); // @phpstan-ignore-line
            return $this->redirect(ConfigHelper::get('splynx_url') . 'portal/login');
        }
        // @phpstan-ignore-next-line
        Yii::$app->getSession()->setFlash(
            'splynx_flash_',
            'danger::Sorry, your email address is not registered!</br> Possible, your email address may not be verified in your service.'
        );
        return $this->redirect(ConfigHelper::get('splynx_url') . 'register/site/auth?authclient=' . $client->getId());
    }
}
