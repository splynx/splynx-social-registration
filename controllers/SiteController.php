<?php

namespace app\controllers;

use app\models\Customers;
use app\models\RequestSend;
use app\models\SplynxCustomer;
use app\models\SplynxCustomers;
use app\models\SplynxLead;
use Exception;
use splynx\base\WebApplication;
use splynx\helpers\ConfigHelper;
use Yii;
use yii\authclient\ClientInterface;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\web\View;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * @return array<mixed>
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null, // @phpstan-ignore-line
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    /**
     * @return array<mixed>|string|Response
     * @throws UserException
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        $id = Yii::$app->session->get('temp_customer_id'); // @phpstan-ignore-line
        $url = ConfigHelper::get('splynx_url') . 'portal/login';
        $templatesBasePath = Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        if ($id !== null) {
            $model = Customers::findOne($id);
            if ($model === null || $model->real_added == Customers::ADDED) {
                $model = new Customers();
            }
        } else {
            $model = new Customers();
        }

        $isSendingNow = ConfigHelper::get('send_verification_code_immediately');
        $isNeedToConfirmEmail = false;

        if ($model->email != null || !ConfigHelper::get('email_verification')) {
            $isNeedToConfirmEmail = true;
        }

        /** @var WebApplication $app */
        $app = Yii::$app;
        $request = $app->request;
        $response = $app->response;

        if ($model->load($request->post())) {
            if ($request->isAjax) {
                $response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            $exists = $model->checkCustomerExists();
            if (!empty($exists)) {
                /** @var View $view */
                $view = $this->getView();
                $view->title = 'Social Registration';
                Yii::$app->getSession()->setFlash('danger', $exists); // @phpstan-ignore-line
                $model->real_added = Customers::IN_PROCESS_SOCIAL;
                $model->email = null;
                return $this->render('index', [
                    'model' => $model,
                    'social' => $model->checkSocialRegistration(),
                    'config' => ConfigHelper::getParams(),
                    'real_added' => $model->real_added,
                ]);
            }
            $model->real_added = Customers::ADDED;
            if ($model->save()) {
                if ($isNeedToConfirmEmail) {
                    $newRealCustomer = SplynxCustomers::get();
                    $result = $newRealCustomer->addCustomer($model);
                    if (!$result['result']) {
                        Yii::$app->getSession()->setFlash('danger', $result['message']); // @phpstan-ignore-line
                        return $this->redirect('index');
                    }
                    $customer = $newRealCustomer->getRealCustomerByAttribute('email', $model->email);
                    if (!empty($customer)) {
                        $sendTo = 'email';
                        $register_as = ConfigHelper::get('register_as');
                        $register_as = ucfirst($register_as);

                        if (ConfigHelper::get('send_verification_code_type') == 'email') {
                            if (file_exists($templatesBasePath . 'email/success' . $register_as . '.twig')) {
                                $templateSuccess = 'email/success' . $register_as . '.twig';
                            } else {
                                $templateSuccess = 'email/success' . $register_as . '.example.twig';
                            }
                            $customerMessage = $this->renderPartial('//../templates/' . $templateSuccess, [
                                'login' => $customer['login'],
                                'password' => $customer['password'],
                                'link' => $url,
                                'domain' => ConfigHelper::get('splynx_url'),
                                'date' => date('Y'),
                            ]);
                            if (file_exists($templatesBasePath . 'email/subjectSuccess' . $register_as . '.twig')) {
                                /** @var string $subject */
                                $subject = file_get_contents($templatesBasePath . 'email/subjectSuccess' . $register_as . '.twig');
                            } else {
                                /** @var string $subject */
                                $subject = file_get_contents($templatesBasePath . 'email/subjectSuccess' . $register_as . '.example.twig');
                            }
                            RequestSend::sendEmail($customer['email'], $subject, $customerMessage, $isSendingNow);
                        } elseif (ConfigHelper::get('send_verification_code_type') == 'sms') {
                            if (file_exists($templatesBasePath . 'sms/success' . $register_as . '.twig')) {
                                $templateSuccess = 'sms/success' . $register_as . '.twig';
                            } else {
                                $templateSuccess = 'sms/success' . $register_as . '.example.twig';
                            }
                            $customerMessage = $this->renderPartial('//../templates/' . $templateSuccess, [
                                'login' => $customer['login'],
                                'password' => $customer['password'],
                                'link' => $url,
                            ]);
                            RequestSend::sendSMS($customer['phone'], $customerMessage, $isSendingNow);
                            $sendTo = 'sms';
                        }
                        $view = 'success' . $register_as;
                        return $this->render($view, [
                            'transport' => $sendTo,
                            'redirect_url' => ConfigHelper::get('redirect_url'),
                        ]);
                    } else {
                        Yii::$app->getSession()->setFlash('danger', 'Error in data search! Please contact our administrator.'); // @phpstan-ignore-line
                        return $this->redirect('index');
                    }
                }
                if (ConfigHelper::get('send_verification_code_type') == 'email') {
                    if (file_exists($templatesBasePath . 'email/confirm.twig')) {
                        $templateConfirm = 'email/confirm.twig';
                    } else {
                        $templateConfirm = 'email/confirm.example.twig';
                    }
                    $mailMessage = $this->renderPartial('//../templates/' . $templateConfirm, [
                        'verification_code' => $model->verification_code,
                        'domain' => ConfigHelper::get('splynx_url'),
                        'date' => date('Y'),
                    ]);
                    if (file_exists($templatesBasePath . 'email/subjectConfirm.twig')) {
                        /** @var string $subject */
                        $subject = file_get_contents($templatesBasePath . 'email/subjectConfirm.twig');
                    } else {
                        /** @var string $subject */
                        $subject = file_get_contents($templatesBasePath . 'email/subjectConfirm.example.twig');
                    }
                    RequestSend::sendEmail($model->email, $subject, $mailMessage, $isSendingNow);
                    $sendTo = 'email';
                } elseif (ConfigHelper::get('send_verification_code_type') == 'sms') {
                    if (file_exists($templatesBasePath . 'sms/confirm.twig')) {
                        $templateConfirm = 'sms/confirm.twig';
                    } else {
                        $templateConfirm = 'sms/confirm.example.twig';
                    }
                    $message = $this->renderPartial('//../templates/' . $templateConfirm, [
                        'domain' => ConfigHelper::get('splynx_url'),
                        'verification_code' => $model->verification_code,
                    ]);
                    RequestSend::sendSMS($model->phone, $message, $isSendingNow);
                    $sendTo = 'sms';
                } else {
                    /** @var View $view */
                    $view = $this->getView();
                    $view->title = 'Error';
                    return $this->render('error_config');
                }
                Yii::$app->getSession()->setFlash('success', 'Check your ' . $sendTo . ' for verification code'); // @phpstan-ignore-line
                return $this->redirect('confirm');
            } else {
                Yii::$app->getSession()->setFlash('danger', json_encode($model->getErrors())); // @phpstan-ignore-line
                return $this->redirect('index');
            }
        }

        /** @var View $view */
        $view = $this->getView();
        $view->title = 'Social Registration';
        return $this->render('index', [
            'model' => $model,
            'social' => $model->checkSocialRegistration(),
            'config' => ConfigHelper::getParams(),
            'real_added' => $model->real_added,
        ]);
    }

    /**
     * @param int|string|null $secret
     * @return string
     * @throws UserException
     */
    public function actionConfirm($secret = null)
    {
        $model = SplynxCustomers::get();
        $model->scenario = 'confirm';
        $isSendingNow = ConfigHelper::get('send_verification_code_immediately');
        $url = ConfigHelper::get('splynx_url') . 'portal/login';

        $templatesBasePath = Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        $model->code = $secret;

        /** @var WebApplication $app */
        $app = Yii::$app;
        $request = $app->request;

        if ($model->load($request->post())) {
            /** @var Customers $customer */
            $customer = Customers::findCustomerByVerificationCode($model->code);
            $result = $model->addCustomer($customer);
            if (!$result['result']) {
                $model->code = null;
                Yii::$app->getSession()->setFlash('danger', $result['message']); // @phpstan-ignore-line
            } else {
                /** @var Customers|null $customer */
                if (!empty($customer)) {
                    $customer = $model->getRealCustomerByAttribute('email', $customer->email);
                }
                if ($customer) {
                    $sendTo = 'email';
                    $register_as = ConfigHelper::get('register_as');
                    $register_as = ucfirst($register_as);

                    if (ConfigHelper::get('send_verification_code_type') == 'email') {
                        if (file_exists($templatesBasePath . 'email/success' . $register_as . '.twig')) {
                            $templateSuccess = 'email/success' . $register_as . '.twig';
                        } else {
                            $templateSuccess = 'email/success' . $register_as . '.example.twig';
                        }

                        $customerMessage = $this->renderPartial('//../templates/' . $templateSuccess, [
                            'login' => $customer['login'],
                            'password' => $customer['password'],
                            'domain' => ConfigHelper::get('splynx_url'),
                            'link' => $url,
                            'date' => date('Y'),
                        ]);
                        if (file_exists($templatesBasePath . 'email/subjectSuccess' . $register_as . '.twig')) {
                            /** @var string $subject */
                            $subject = file_get_contents($templatesBasePath . 'email/subjectSuccess' . $register_as . '.twig');
                        } else {
                            /** @var string $subject */
                            $subject = file_get_contents($templatesBasePath . 'email/subjectSuccess' . $register_as . '.example.twig');
                        }
                        RequestSend::sendEmail($customer['email'], $subject, $customerMessage, $isSendingNow);
                    } elseif (ConfigHelper::get('send_verification_code_type') == 'sms') {
                        if (file_exists($templatesBasePath . 'sms/success' . $register_as . '.twig')) {
                            $templateSuccess = 'sms/success' . $register_as . '.twig';
                        } else {
                            $templateSuccess = 'sms/success' . $register_as . '.example.twig';
                        }
                        $customerMessage = $this->renderPartial('//../templates/' . $templateSuccess, [
                            'login' => $customer['login'],
                            'password' => $customer['password'],
                            'link' => $url,
                        ]);
                        RequestSend::sendSMS($customer['phone'], $customerMessage, $isSendingNow);
                        $sendTo = 'sms';
                    }
                    $view = 'success' . $register_as;
                    return $this->render($view, [
                        'transport' => $sendTo,
                        'redirect_url' => ConfigHelper::get('redirect_url'),
                    ]);
                } else {
                    $model->code = null;
                    Yii::$app->getSession()->setFlash('danger', 'Error in data search! Please contact our administrator.'); // @phpstan-ignore-line
                }
            }
        }
        /** @var View $view */
        $view = $this->getView();
        $view->title = 'Confirm';
        return $this->render('confirm', [
            'model' => $model,
        ]);
    }

    /**
     * This function will be triggered when user is successful authenticated using some oAuth client.
     *
     * @param ClientInterface $client
     * @return void
     * @throws Exception
     */
    public function onAuthSuccess($client)
    {
        $source = $client->getId();
        $attributes = $client->getUserAttributes();

        switch ($source) {
            case 'google':
                $this->userHandler($attributes, Customers::GOOGLE_PREFIX, 'email', 'name');
                break;
            case 'twitter':
                $this->userHandler($attributes, Customers::TWITTER_PREFIX, 'email', 'name');
                break;
            case 'facebook':
                $this->userHandler($attributes, Customers::FACEBOOK_PREFIX, 'email', 'name');
                break;
        }
    }

    /**
     * @return Response
     */
    public function actionSendCodeAgain()
    {
        /** @var WebApplication $app */
        $app = \Yii::$app;
        $session = $app->getSession();
        $id = $session->get('message_id');
        if ($id == null) {
            $session->setFlash('danger', 'Session expired! Try again.');
            return $this->redirect('index');
        }
        RequestSend::sendMessageAgain($id);
        $session->setFlash('success', 'Code send to you');
        return $this->redirect('confirm');
    }

    /**
     * @param string $socialId
     * @return SplynxCustomer|SplynxLead|null
     */
    private function getIdentity($socialId)
    {
        return (SplynxCustomers::get())->getCustomerBySocialId($socialId);
    }

    /**
     * @param array<mixed> $attributes
     * @param string $prefix
     * @param string $email_format
     * @param string $name_format
     * @return Response
     * @throws Exception
     */
    protected function userHandler($attributes, $prefix, $email_format, $name_format)
    {
        $model = Customers::findOne(['social_id' => $prefix . $attributes['id']]);
        if ($model == null) {
            $model = new Customers();
        }
        $identity = $this->getIdentity($prefix . $attributes['id']);
        if (!$identity) {
            $social_email = ArrayHelper::getValue($attributes, $email_format);
            $social_name = ArrayHelper::getValue($attributes, $name_format);
            /** @var SplynxCustomer|SplynxLead|null $customer */
            $customer = (SplynxCustomers::get())->getRealCustomerByAttribute('email', $social_email);
            if (isset($customer->email) && $customer->email == $social_email) {
                if ($customer->setSocialIdForCustomer($prefix . $attributes['id'])) {
                    Yii::$app->session->set('splynx_customer_id', $customer['id']); // @phpstan-ignore-line
                    return $this->redirect(ConfigHelper::get('splynx_url') . 'portal');
                }
            }

            $model->email = $social_email;
            if (ConfigHelper::get('form_login')) {
                $model->login = $social_email;
            }
            $model->full_name = $social_name;
            $model->social_id = $prefix . $attributes['id'];
        } else {
            Yii::$app->session->set('splynx_customer_id', $identity['id']); // @phpstan-ignore-line
            return $this->redirect(ConfigHelper::get('splynx_url') . 'portal');
        }

        $model->real_added = Customers::IN_PROCESS_SOCIAL;

        if ($model->save(false)) {
            Yii::$app->session->set('temp_customer_id', $model->id); // @phpstan-ignore-line
            return $this->redirect(['index']);
        } else {
            throw new InvalidCallException('Error save to data.db!');
        }
    }

    /**
     * Use to enable buttons from config for code in portal
     *
     * @return array<string, mixed>
     */
    public function actionSettings()
    {
        /** @var WebApplication $app */
        $app = Yii::$app;
        $response = $app->response;
        $response->format = Response::FORMAT_JSON;
        return [
            'google' => ConfigHelper::get('google_enable'),
            'facebook' => ConfigHelper::get('facebook_enable'),
            'x' => ConfigHelper::get('twitter_enable'),
        ];
    }

    /**
     * @return string|\yii\console\Response|Response
     */
    public function actionLicense()
    {
        if (ConfigHelper::get('form_license_agreement')) {
            $file = ConfigHelper::get('license_agreement_file_path');
            if (!file_exists($file)) {
                return $this->render('error');
            }
            /** @var string $typeArray */
            $typeArray = mime_content_type($file);
            $type = explode('/', $typeArray);
            if (isset($type[0]) && $type[0] == 'text') {
                return $this->renderPartial('license', [
                    'license' => file_get_contents($file),
                ]);
            } else {
                /** @var WebApplication $app */
                $app = Yii::$app;
                $response = $app->response;
                return $response->sendFile($file, basename($file));
            }
        } else {
            return $this->render('error');
        }
    }

    /**
     * @param int|null|string $partner
     * @return array<string, array<int|empty, string|empty>>
     */
    public function actionGetTariffs($partner = null)
    {
        $model = new Customers();
        /** @var WebApplication $app */
        $app = Yii::$app;
        $response = $app->response;
        $response->format = Response::FORMAT_JSON;
        return [
            'internet' => $model->getAvailableTariffsList($partner),
            'voice' => $model->getAvailableVoiceTariffsList($partner),
            'custom' => $model->getAvailableCustomTariffsList($partner),
        ];
    }
}
