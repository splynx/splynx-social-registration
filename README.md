Splynx Social Registration Add-on
======================

Splynx Social Registration Add-on based on [Yii2](http://www.yiiframework.com).

REQUIREMENTS
------------

Installed [Splynx ISP Framework](https://www.splynx.com)

Install base add-on:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base-2.git && cd splynx-addon-base-2
composer install
~~~
Installed PHP-Sqlite extension

Install Splynx Social Registration Add-on:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-social-registration.git
cd splynx-social-registration
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-social-registration/web/ /var/www/splynx/web/register
~~~

Create Nginx config file `/etc/nginx/sites-available/splynx-social-registration.addons`:
~~~
location /register
{
        try_files $uri $uri/ /register/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

Settings available in `/var/www/splynx/addons/splynx-social-registration/config/params.php`.

Messages templates in `/var/www/splynx/addons/splynx-social-registration/templates`.

Custom style
~~~
cp /var/www/splynx/addons/splynx-social-registration/views/site/base.index.twig /var/www/splynx/addons/splynx-social-registration/views/site/custom.index.twig 
cp /var/www/splynx/addons/splynx-social-registration/views/site/base_customer.success.twig /var/www/splynx/addons/splynx-social-registration/views/site/custom_customer.success.twig
cp /var/www/splynx/addons/splynx-social-registration/views/site/base_lead.success.twig /var/www/splynx/addons/splynx-social-registration/views/site/custom_lead.success.twig
cp /var/www/splynx/addons/splynx-social-registration/views/site/base.confirm.twig /var/www/splynx/addons/splynx-social-registration/views/site/custom.confirm.twig
cp /var/www/splynx/addons/splynx-social-registration/web/css/site.css /var/www/splynx/addons/splynx-social-registration/web/css/custom.site.css
cp /var/www/splynx/addons/splynx-social-registration/views/layouts/base.main.php /var/www/splynx/addons/splynx-social-registration/views/layouts/custom.main.php
~~~

~~~
then edit next files for your custom style:
'.../views/site/custom.index.twig',
'.../views/site/custom.confirm.twig',
'.../views/site/custom_customer.success.twig',
'.../views/site/custom_lead.success.twig',
'.../web/css/custom.site.css'
'.../views/layouts/custom.main.php'
~~~

Then you can access Splynx Social Registration Add-on in path "YOUR_SPLYNX_DOMAIN/register/" or "YOUR_SPLYNX_DOMAIN/portal/login/".