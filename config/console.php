<?php

return function ($params, $baseDir) {
    return [
        'api' => [
            'version' => SplynxApi::API_VERSION_2,
        ],
    ];
};
