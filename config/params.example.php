<?php

// First of all create API key and set permissions
return [
    'api_domain' => '', // Please set your Splynx URL http://splynx/
    'api_key' => '', // Please set your API_KEY
    'api_secret' => '', // Please set your API_SECRET

    'send_verification_code_type' => 'email', // Set verification code and registration data send type: sms or email
    'send_verification_code_immediately' => false, // Set true or false

    'splynx_url' => '', // Your splynx url  http://example.com/ (with slash in end)

    // Create app in your google api
    'google_enable' => true, // Enable google button
    'google_client_id' => '', // Set client id from your google app
    'google_client_secret' => '', // Set client secret from your google app

    // Create app in your facebook api
    'facebook_enable' => true, // Enable facebook button
    'facebook_client_id' => '', // Set client id from your facebook app
    'facebook_client_secret' => '', // Set client secret from your facebook app

    // Create app in your twitter api
    'twitter_enable' => true, // Enable twitter button
    'twitter_customer_key' => '', // Set customer key from your twitter app
    'twitter_customer_secret' => '', // Set customer secret from your twitter app

    // Set billing type for created customers
    // Values: 'prepaid' or 'recurring'
    'billing_type' => 'recurring',

    // Partner id set for self registered customers
    'partner_id' => 1,

    // Location id set for self registered customers
    'location_id' => 1,

    'form_enable' => true, // Enable or disable form for using data from social
    // Set fields for display
    'form_login' => false, // If enabled user can enter login itself, else login will be generated
    'form_phone' => true,
    'form_zip_code' => true,
    'form_city' => true,
    'form_street' => true,
    'form_internet_tariff' => true,
    // Use license agreement
    'form_license_agreement' => false,

    // Agreement file local path (html, txt, pdf ...)
    'license_agreement_file_path' => '/tmp/license.html',

    // Create contract by splynx template contract_pdf_template_id, add contract to customer documents and sent on customer email
    'create_customer_contract' => false,
    // Directory for contract pdf generation temporary files before upload to splynx
    // (!!! with last slash and read/write permissions, if not set or not exists add-on use default /tmp/)
    'contract_pdf_path' => '/tmp/',
    // Contract email template id from splynx (type: Mail) for message in contract mail (if not set add-on will use default text)
    // For example you can use template ADD-ON_DIR/templates/contract_mail.twig
    'contract_email_template_id' => 0,
    // Contract pdf template id from splynx (type: Document) for create PDF contract
    // For example you can use template ADD-ON_DIR/templates/contract_pdf.twig
    'contract_pdf_template_id' => 0,
    // If you want to sent notifications about new customer set it true and set admin id
    'admin_notification' => false,
    // Splynx admin id for send notification about new customer
    'splynx_admin_id' => 1,

    'cookieValidationKey' => '',
];
