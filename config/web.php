<?php

use app\models\Administrator;

return function ($params, $baseDir) {
    $clients = [];

    if ($params['facebook_enable'] == true) {
        $clients['facebook'] = [
            'class' => 'yii\authclient\clients\Facebook',
            'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
            'clientId' => $params['facebook_client_id'],
            'clientSecret' => $params['facebook_client_secret'],
            'attributeNames' => [
                'id',
                'name',
                'first_name',
                'last_name',
                'email',
            ],
        ];
    }
    if ($params['google_enable'] == true) {
        $clients['google'] = [
            'class' => 'yii\authclient\clients\Google',
            'clientId' => $params['google_client_id'],
            'clientSecret' => $params['google_client_secret'],
        ];
    }
    if ($params['twitter_enable'] == true) {
        $clients['twitter'] = [
            'class' => 'app\models\twitter\Twitter',
            'consumerKey' => $params['twitter_customer_key'],
            'consumerSecret' => $params['twitter_customer_secret'],
            'attributeParams' => [
                'include_email' => 'true',
            ],
        ];
    }

    return [
        'api' => [
            'version' => SplynxApi::API_VERSION_2,
        ],
        'components' => [
            'request' => [
                'baseUrl' => '/register',
            ],
            'user' => [
                'identityClass' => Administrator::class,
            ],
            'view' => [
                'renderers' => [
                    'twig' => [
                        'globals' => [
                            'AuthChoice' => '\yii\authclient\widgets\AuthChoice',
                        ],
                    ],
                ],
            ],
            'authClientCollection' => [
                'class' => 'yii\authclient\Collection',
                'clients' => $clients,
            ],
        ],
    ];
};
