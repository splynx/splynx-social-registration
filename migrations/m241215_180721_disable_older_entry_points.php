<?php

use splynx\models\console_api\config\integration\ConsoleModuleEntryPoint;
use yii\db\Migration;

/**
 * Class m241215_180721_disable_older_entry_points
 */
class m241215_180721_disable_older_entry_points extends Migration
{
    /** @var string */
    public const MODULE = 'splynx_addon_social_registration';

    /**
     * {@inheritdoc}
     */
    public function up()
    {
        return $this->toggleEntryPoints();
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        return $this->toggleEntryPoints(true);
    }

    /**
     * @return array<string>
     */
    private static function getEntrypointNames(): array
    {
        return ['splynx_social_registration_new', 'splynx_social_registration_register'];
    }

    /**
     * @param bool $isEnabled
     * @return bool
     */
    private function toggleEntryPoints(bool $isEnabled = false): bool
    {
        foreach (self::getEntrypointNames() as $name) {
            $model = (new ConsoleModuleEntryPoint())->findOne([
                'module' => static::MODULE,
                'name' => $name, 
            ]);

            if (empty($model) || (bool)$model->enabled === $isEnabled) {
                continue;
            }

            $model->enabled = $isEnabled;
            if (!$model->save()) {
                print "Could not save entrypoint: '$model->name'\n";
                if ($model->hasErrors()) {
                    print implode(PHP_EOL, $model->getErrorSummary(true));
                    print PHP_EOL;
                }

                return false;
            }
        }

        return true;
    }
}
