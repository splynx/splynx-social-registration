<?php

use splynx\models\console_api\config\integration\ConsoleModuleEntryPoint;
use yii\db\Migration;

/**
 * Class m200727_151822_update_entry_points
 */
class m200727_151822_update_entry_points extends Migration
{
    /**
     * @inheritDoc
     */
    public function up()
    {
        $path = Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'entry-point' . DIRECTORY_SEPARATOR;
        $entryPoints = [
            'splynx_social_registration' => 'portal.twig',
            'splynx_social_registration_register' => 'register.twig',
        ];

        foreach ($entryPoints as $name => $file) {
            $entryPoint = (new ConsoleModuleEntryPoint())->findOne(
                [
                    'name' => $name,
                    'place' => ConsoleModuleEntryPoint::PLACE_PORTAL,
                    'type' => ConsoleModuleEntryPoint::TYPE_CODE,
                ]);
            if (empty($entryPoint)) {
                continue;
            }
            /** @var string $code */
            $code = file_get_contents($path . $file);
            if (md5($entryPoint->code) === md5($code)) {
                continue;
            }
            /** @var string $code */
            $code = file_get_contents($path . $file);
            $entryPoint->code = urlencode($code);
            $entryPoint->save();
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        echo "m200727_151822_update_entry_points cannot be reverted.\n";

        return false;
    }
}
