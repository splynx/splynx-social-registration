<?php

use yii\db\Migration;

/**
 * Class m180502_141447_notes_column_for_customers
 */
class m180502_141447_notes_column_for_customers extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customers', 'comment', $this->string());

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m180502_141447_notes_column_for_customers cannot be reverted.\n";

        return false;
    }
}
