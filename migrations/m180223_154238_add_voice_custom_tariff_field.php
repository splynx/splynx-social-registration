<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m180223_154238_add_voice_custom_tariff_field
 */
class m180223_154238_add_voice_custom_tariff_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customers', 'voice_tariff', Schema::TYPE_INTEGER);
        $this->addColumn('customers', 'custom_tariff', Schema::TYPE_INTEGER);

        return true;
    }

    /**
     * @inheridoc
     */
    public function down()
    {
        echo "m180223_154238_add_voice_custom_tariff_field cannot be reverted.\n";

        return false;
    }
}
