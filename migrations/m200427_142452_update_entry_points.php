<?php

use yii\db\Migration;

/**
 * Class m200427_142452_update_entry_points
 */
class m200427_142452_update_entry_points extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $path = Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'entry-point' . DIRECTORY_SEPARATOR;
        $entryPoints = [
            'splynx_social_registration' => 'portal.twig',
            'splynx_social_registration_register' => 'register.twig',
        ];

        foreach ($entryPoints as $name => $file) {
            $entryPoint = (new \splynx\models\console_api\config\integration\ConsoleModuleEntryPoint())->findOne(
                [
                    'name' => $name,
                    'place' => \splynx\models\console_api\config\integration\ConsoleModuleEntryPoint::PLACE_PORTAL,
                    'type' => \splynx\models\console_api\config\integration\ConsoleModuleEntryPoint::TYPE_CODE,
                ]);
            if (empty($entryPoint)) {
                continue;
            }
            /** @var string $code */
            $code = file_get_contents($path . $file . '.backup_old');
            if (md5($entryPoint->code) != md5($code)) {
                continue;
            }
            /** @var string $code */
            $code = file_get_contents($path . $file);
            $entryPoint->code = urlencode($code);
            $entryPoint->save();
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m200427_142452_update_entry_points cannot be reverted.\n";
        return false;
    }
}
