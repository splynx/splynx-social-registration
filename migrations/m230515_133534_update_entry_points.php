<?php

use splynx\models\console_api\config\integration\ConsoleModuleEntryPoint;
use yii\db\Migration;

/**
 * Class m230515_133534_update_entry_points
 */
class m230515_133534_update_entry_points extends Migration
{
    public const MODULE = 'splynx_addon_social_registration';
    public const NAME = 'splynx_social_registration';

    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        return $this->setModuleVisibility(false);
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        return $this->setModuleVisibility(true);
    }

    /**
     * Sets module visibility
     *
     * @param boolean $isVisible
     * @return boolean
     */
    private function setModuleVisibility(bool $isVisible): bool
    {
        $model = (new ConsoleModuleEntryPoint())->findOne([
            'module' => static::MODULE,
            'name' => static::NAME,
            'type' => ConsoleModuleEntryPoint::TYPE_CODE,
        ]);

        if (!$model) {
            print 'Could not find entrypoint: ' . static::NAME;
            return true;
        }

        if ((bool)$model->enabled === $isVisible) {
            return true;
        }

        $model->enabled = $isVisible;

        if (!$model->save()) {
            print "Could not save entrypoint: '$model->name'";
            return false;
        }

        return true;
    }
}
