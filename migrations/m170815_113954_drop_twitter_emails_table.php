<?php

use yii\db\Migration;
use yii\db\sqlite\Schema;

/**
 * Handles the dropping of table `twitter_emails`.
 */
class m170815_113954_drop_twitter_emails_table extends Migration
{
    /**
     * @inheridoc
     */
    public function up()
    {
        $this->dropTable('twitter');

        return true;
    }

    /**
     * @inheridoc
     */
    public function down()
    {
        $this->createTable('twitter', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING,
            'verification_code' => Schema::TYPE_INTEGER,
            'full_name' => Schema::TYPE_STRING,
            'city' => Schema::TYPE_STRING,
            'login' => Schema::TYPE_STRING,
            'social_id' => Schema::TYPE_STRING
        ]);

        return true;
    }
}
