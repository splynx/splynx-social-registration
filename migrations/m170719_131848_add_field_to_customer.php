<?php

use yii\db\Migration;
use yii\db\Schema;

class m170719_131848_add_field_to_customer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customers', 'internet_tariff', Schema::TYPE_INTEGER);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('customers', 'internet_tariff');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
