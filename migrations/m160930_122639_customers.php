<?php

use yii\db\Migration;
use yii\db\sqlite\Schema;


class m160930_122639_customers extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customers', [
            'id' => Schema::TYPE_PK,
            'billing_type' => Schema::TYPE_STRING,
            'partner_id' => Schema::TYPE_INTEGER,
            'location_id' => Schema::TYPE_INTEGER,
            'added_by' => Schema::TYPE_STRING,
            'added_by_id' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_STRING,
            'login' => Schema::TYPE_STRING,
            'category' => Schema::TYPE_STRING,
            'password' => Schema::TYPE_STRING,
            'full_name' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'phone' => Schema::TYPE_STRING,
            'street' => Schema::TYPE_STRING,
            'zip_code' => Schema::TYPE_STRING,
            'city' => Schema::TYPE_STRING,
            'date_add' => Schema::TYPE_STRING,
            'verification_code' => Schema::TYPE_INTEGER,
            'social_id' => Schema::TYPE_STRING,
            'real_added' => Schema::TYPE_STRING
        ]);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('customers');

        return true;
    }
}
