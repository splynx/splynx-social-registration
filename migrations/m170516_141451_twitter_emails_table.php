<?php

use yii\db\Migration;
use yii\db\sqlite\Schema;

class m170516_141451_twitter_emails_table extends Migration
{
    /**
     * @inheridoc
     */
    public function up()
    {
        $this->createTable('twitter', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING,
            'verification_code' => Schema::TYPE_INTEGER,
            'full_name' => Schema::TYPE_STRING,
            'city' => Schema::TYPE_STRING,
            'login' => Schema::TYPE_STRING,
            'social_id' => Schema::TYPE_STRING
        ]);

        return true;
    }

    /**
     * @inheridoc
     */
    public function down()
    {
        $this->dropTable('twitter');

        return true;
    }
}
