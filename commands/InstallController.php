<?php

namespace app\commands;

use splynx\base\BaseInstallController;
use splynx\models\console_api\config\integration\ConsoleModuleEntryPoint;
use Yii;
use yii\helpers\ArrayHelper;

class InstallController extends BaseInstallController
{
    public function getAddOnTitle()
    {
        return 'Splynx Social Registration Add-on';
    }

    public function getModuleName()
    {
        return 'splynx_addon_social_registration';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\config\Mail',
                'actions' => ['index', 'view', 'add', 'update'],
            ],
            [
                'controller' => 'api\admin\config\Sms',
                'actions' => ['index', 'view', 'add', 'update'],
            ],
            [
                'controller' => 'api\admin\config\AdditionalFields',
                'actions' => ['index', 'view', 'add'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'view', 'add', 'update'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerInfo',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerViewPassword',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerInternetServices',
                'actions' => ['index', 'view', 'add'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerVoiceServices',
                'actions' => ['index', 'view', 'add'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerCustomServices',
                'actions' => ['index', 'view', 'add'],
            ],
            [
                'controller' => 'api\admin\tariffs\Internet',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\tariffs\Voice',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\tariffs\Custom',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\SendDocuments',
                'actions' => ['index', 'add'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerDocuments',
                'actions' => ['index', 'view', 'add', 'update'],
            ],
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\config\Templates',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\administration\Partners',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\administration\Locations',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\crm\Leads',
                'actions' => ['index', 'add', 'view'],
            ],
            [
                'controller' => 'api\admin\crm\LeadsInfo',
                'actions' => ['index', 'view', 'update'],
            ],
        ];
    }

    /**
     * @return array<int, array<string, bool|string>>
     */
    public function getAdditionalFields()
    {
        return [
            [
                'main_module' => 'customers',
                'name' => 'social_id',
                'title' => 'Social ID',
                'type' => 'string',
                'required' => false,
                'is_add' => true,
            ],
            [
                'main_module' => 'customers',
                'name' => 'self_registration_comment',
                'title' => 'Comment',
                'type' => 'textarea',
                'required' => false,
                'is_add' => false,
            ],
            [
                'main_module' => 'tariffs_internet',
                'name' => 'available_when_register_by_social_network',
                'title' => 'Available when register by social network',
                'type' => 'boolean',
                'required' => false,
                'is_add' => true,
            ],
            [
                'main_module' => 'tariffs_custom',
                'name' => 'available_in_self_registration',
                'title' => 'Available in self-registration',
                'type' => 'boolean',
                'required' => false,
                'is_add' => true,
            ],
            [
                'main_module' => 'tariffs_voice',
                'name' => 'available_in_self_registration',
                'title' => 'Available in self-registration',
                'type' => 'boolean',
                'required' => false,
                'is_add' => true,
            ],
            [
                'main_module' => 'partners',
                'name' => 'available_in_self_registration',
                'title' => 'Available in self-registration',
                'type' => 'boolean',
                'required' => false,
                'is_add' => true,
            ],
            [
                'main_module' => 'locations',
                'name' => 'available_in_self_registration',
                'title' => 'Available in self-registration',
                'type' => 'boolean',
                'required' => false,
                'is_add' => true,
            ],
        ];
    }

    /**
     * @return array<int, array<string, bool|string>>
     */
    public function getEntryPoints()
    {
        /** @var string $code */
        $code = file_get_contents(
            Yii::$app->getBasePath(
            ) . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'entry-point' . DIRECTORY_SEPARATOR . 'portal.twig'
        );
        /** @var string $codeRegister */
        $codeRegister = file_get_contents(
            Yii::$app->getBasePath()
            . DIRECTORY_SEPARATOR . 'templates'
            . DIRECTORY_SEPARATOR . 'entry-point'
            . DIRECTORY_SEPARATOR . 'register.twig'
        );
        return [
            [
                'name' => 'splynx_social_registration_redesigned',
                'title' => 'Splynx Social Login',
                'place' => ConsoleModuleEntryPoint::PLACE_PORTAL,
                'type' => ConsoleModuleEntryPoint::TYPE_CODE,
                'code' => urlencode($code),
                'root' => 'controllers\portal\LoginController',
                'enabled' => true,
            ],
            [
                'name' => 'splynx_social_registration_register_redesigned',
                'title' => 'Splynx Social Registration',
                'place' => ConsoleModuleEntryPoint::PLACE_PORTAL,
                'type' => ConsoleModuleEntryPoint::TYPE_CODE,
                'code' => urlencode($codeRegister),
                'root' => 'controllers\portal\LoginController',
                'enabled' => false,
            ],
        ];
    }

    public function getFilesPermission()
    {
        return ArrayHelper::merge(parent::getFilesPermission(), ['data' . DIRECTORY_SEPARATOR . 'data.db' => '0777']);
    }
}
