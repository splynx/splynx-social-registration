<?php

if (file_exists(__DIR__ . '/custom.main.php')) {
    require('custom.main.php');
} else {
    require('base.main.php');
}
