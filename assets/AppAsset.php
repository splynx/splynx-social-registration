<?php

namespace app\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\View;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    /** @var string[] */
    public $css;
    /** @var array<string, int> */
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
    /** @var string[] */
    public $js = [
        'js/jquery-1.12.4.js',
    ];
    /** @var string[] */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    /**
     * @return void
     */
    public function init()
    {
        $this->css = [
            file_exists(Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'custom.site.css') ?
                'css/custom.site.css' :
                'css/site.css',
                'css/bootstrap-social.css',
                'css/font-awesome.css',
        ];
    }
}
