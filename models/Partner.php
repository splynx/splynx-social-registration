<?php

namespace app\models;

use splynx\v2\models\administration\BasePartner;

class Partner extends BasePartner
{
    /**
     * Available partners for select in self-registration
     * @return Partner[]
     */
    public function getAvailablePartners()
    {
        return $this->findAll([], ['available_in_self_registration' => '1']);
    }
}
