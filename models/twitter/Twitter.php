<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\models\twitter;

use InvalidArgumentException;
use splynx\base\WebApplication;
use Yii;
use yii\authclient\OAuth1;
use yii\web\HttpException;

class Twitter extends OAuth1
{
    /**
     * {@inheritdoc}
     */
    public $authUrl = 'https://api.twitter.com/oauth/authenticate';
    /**
     * {@inheritdoc}
     */
    public $requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
    /**
     * {@inheritdoc}
     */
    public $requestTokenMethod = 'POST';
    /**
     * {@inheritdoc}oauth_verifier
     */
    public $accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
    /**
     * {@inheritdoc}
     */
    public $accessTokenMethod = 'POST';
    /**
     * {@inheritdoc}
     */
    public $apiBaseUrl = 'https://api.twitter.com/1.1';
    /**
     * @var array<mixed> list of extra parameters, which should be used, while requesting user attributes from Twitter API.
     * For example:
     *
     * ```php
     * [
     *     'include_email' => 'true'
     * ]
     * ```
     *
     * @see https://dev.twitter.com/rest/reference/get/account/verify_credentials
     * @since 2.0.6
     */
    public $attributeParams = [];

    /**
     * @param array<mixed> $params
     * @return mixed
     */
    public function fetchRequestToken(array $params = [])
    {
        $this->setAccessToken(null);
        $defaultParams = [
            'oauth_consumer_key' => $this->consumerKey,
            'oauth_callback' => $this->getReturnUrl(),
            // 'xoauth_displayname' => Yii::$app->name,
        ];
        if (!empty($this->scope)) {
            $defaultParams['scope'] = $this->scope;
        }

        $request = $this->createRequest()
            ->setMethod($this->requestTokenMethod)
            ->setUrl($this->requestTokenUrl)
            ->setData(array_merge($defaultParams, $params));

        $this->signRequest($request);

        $response = $this->sendRequest($request);

        $token = $this->createToken([
            'params' => $response,
        ]);
        $this->setState('requestToken', $token);

        $session = Yii::$app->session; // @phpstan-ignore-line
        $session->set('oauth_token', $token->getToken());
        $session->set('oauth_token_secret', $token->getTokenSecret());

        return $token;
    }

    /**
     * @param string|null $oauthToken
     * @param null $requestToken
     * @param string|null $oauthVerifier
     * @param array<mixed> $params
     * @return mixed
     */
    public function fetchAccessToken($oauthToken = null, $requestToken = null, $oauthVerifier = null, array $params = [])
    {
        /** @var WebApplication $app */
        $app = Yii::$app;
        $incomingRequest = $app->getRequest();

        if ($oauthToken === null) {
            $oauthToken = $incomingRequest->get('oauth_token', $incomingRequest->post('oauth_token', $oauthToken));
        }

        /** @var array<string,mixed> $session */
        $session = $app->session;
        $response = [
            'oauth_token' => $session['oauth_token'] ?? '',
            'oauth_token_secret' => $session['oauth_token_secret'] ?? '',
            'oauth_callback_confirmed' => true,
        ];
        $requestToken = $this->createToken([
            'params' => $response,
        ]);

        if (!is_object($requestToken)) {
            $requestToken = $this->getState('requestToken');
            if (!is_object($requestToken)) {
                throw new InvalidArgumentException('Request token is required to fetch access token!');
            }
        }

        if (strcmp($requestToken->getToken(), $oauthToken) !== 0) {
            throw new HttpException(400, 'Invalid auth state parameter.');
        }

        $this->removeState('requestToken');

        $defaultParams = [
            'oauth_consumer_key' => $this->consumerKey,
            'oauth_token' => $requestToken->getToken(),
        ];
        if ($oauthVerifier === null) {
            $oauthVerifier = $incomingRequest->get('oauth_verifier', $incomingRequest->post('oauth_verifier'));
        }
        if (!empty($oauthVerifier)) {
            $defaultParams['oauth_verifier'] = $oauthVerifier;
        }

        $request = $this->createRequest()
            ->setMethod($this->accessTokenMethod)
            ->setUrl($this->accessTokenUrl)
            ->setData(array_merge($defaultParams, $params));

        $this->signRequest($request, $requestToken);

        $response = $this->sendRequest($request);

        $token = $this->createToken([
            'params' => $response,
        ]);
        $this->setAccessToken($token);

        return $token;
    }

    /**
     * @return array<mixed>
     */
    protected function initUserAttributes()
    {
        return $this->api('account/verify_credentials.json', 'GET', $this->attributeParams);
    }

    /**
     * {@inheritdoc}
     */
    protected function defaultName()
    {
        return 'twitter';
    }

    /**
     * {@inheritdoc}
     */
    protected function defaultTitle()
    {
        return 'Twitter';
    }
}
