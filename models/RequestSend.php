<?php

namespace app\models;

use splynx\base\ApiResponseException;
use splynx\base\BaseApiModel;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\administration\BaseAdministrator;
use splynx\v2\models\transport\BaseMail;
use splynx\v2\models\transport\BaseMessage;
use splynx\v2\models\transport\BaseSms;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\base\UserException;

class RequestSend extends BaseApiModel
{
    /**
     * @param string|null $recipient
     * @param string $subject
     * @param string $message
     * @param bool $send
     * @return int|null
     * @throws \yii\base\UserException
     */
    public static function sendEmail($recipient, $subject, $message, $send = true)
    {
        $params = [
            'type' => 'add-on',
            'recipient' => $recipient,
            'subject' => $subject,
            'message' => $message,
        ];
        $mail = new BaseMail($params);
        return self::processSend($mail, $send);
    }

    /**
     * @param string|null $recipient
     * @param string $message
     * @param bool $send
     * @return int|null
     * @throws \yii\base\UserException
     */
    public static function sendSMS($recipient, $message, $send = true)
    {
        $params = [
            'type' => 'add-on',
            'recipient' => $recipient,
            'message' => $message,
        ];
        $sms = new BaseSms($params);
        return self::processSend($sms, $send);
    }

    /**
     * message send processing
     * @param BaseMessage $message
     * @param bool $send
     * @return int|null
     * @throws \yii\base\UserException
     */
    public static function processSend(BaseMessage $message, $send)
    {
        $isSend = $message->send($send);
        \Yii::$app->session->set('message_id', $isSend ? $message->id : null); // @phpstan-ignore-line
        return $isSend ? $message->id : null;
    }

    /**
     * @param int|string $messageId
     * @return void
     * @throws ApiResponseException
     * @throws InvalidConfigException
     * @throws UserException
     */
    public static function sendMessageAgain($messageId)
    {
        $isSendingNow = ConfigHelper::get('send_verification_code_immediately');
        if (ConfigHelper::get('send_verification_code_type') == 'email') {
            $email = (new BaseMail())->findById($messageId);
            if (!$email) {
                throw new InvalidCallException('Error in API Call while try to get previous email!');
            }
            $email->status = BaseMessage::STATUS_NEW;
            $email->send($isSendingNow);
        } elseif (ConfigHelper::get('send_verification_code_type') == 'sms') {
            $sms = (new BaseSms())->findById($messageId);
            if (!$sms) {
                throw new InvalidCallException('Error in API Call while try to get previous email!');
            }
            $sms->status = BaseMessage::STATUS_NEW;
            $sms->send($isSendingNow);
        } else {
            throw new InvalidCallException('Error in send method config! Check params.php');
        }
    }

    /**
     * Send notification for admin about new customer
     *
     * @param string|integer $adminId splynx admin id
     * @param string|integer $customerId splynx customer id
     * @param string $url splynx url
     * @param bool $isService is splynx internet service created successfully  by self-registration
     * @param bool $isContract is customer contract document created
     * @return bool|int
     */
    public static function adminNotification($adminId, $customerId, $url, $isService = false, $isContract = false)
    {

        if (!($admin = (new BaseAdministrator())->findById($adminId))) {
            return false;
        }

        $customerCategory = ConfigHelper::get('register_as');
        $contract = $isContract ? '<br><br><a href="' . $url . 'admin/customers/documents?id=' . $customerId . '">' . $customerCategory . ' Documents page</a><br><br>' : '';
        $service = $isService ? '<br><br><a href="' . $url . 'admin/customers/services?id=' . $customerId . '">' . $customerCategory . ' Services page</a><br>' : '';
        $dateTime = date('Y-m-d H:i:s');

        if ($customerCategory == 'lead') {
            $customer = '<br><br><a href="' . $url . 'admin/crm/leads/view?id=' . $customerId . '">' . $customerCategory . ' page</a><br>';
        } else {
            $customer = '<br><br><a href="' . $url . 'admin/customers/view?id=' . $customerId . '">' . $customerCategory . ' page</a><br>';
        }

        $message = <<<HTML
            <!DOCTYPE html>
            <html>
                <body>
                    <div>
                        <h4>New $customerCategory registered in the system through the self-registration add-on (id: $customerId)</h4>
                        $customer
                        $service
                        $contract
                        <br><br><br><br>
                        $dateTime
                    </div>
                </body>
            </html>
            HTML;
        // Add to mailPool
        $params = [
            'type' => 'add-on',
            'recipient' => $admin->email,
            'subject' => 'Self-registration add-on: new customer in the system',
            'message' => $message,
        ];
        $mail = new BaseMail($params);
        if (!$mail->save()) {
            throw new InvalidCallException('Error while adding mail pool by API!');
        }

        return $mail->id;
    }
}
