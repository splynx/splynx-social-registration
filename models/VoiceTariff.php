<?php

namespace app\models;

use app\models\traits\TariffsTrait;
use splynx\v2\models\tariffs\BaseVoiceTariff;

/**
 * Class VoiceTariff
 * @package app\models
 */
class VoiceTariff extends BaseVoiceTariff
{
    use TariffsTrait;

    /**
     * @return VoiceTariff[]
     */
    public function getAvailableVoiceTariffs()
    {
        return $this->findAll($this->getMainConditionForAvailableTariffs(), [
            'available_in_self_registration' => '1',
        ]);
    }
}
