<?php

namespace app\models;

use splynx\base\ApiResponseException;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\customer\BaseCustomer;
use splynx\v2\models\services\BaseService;
use yii\base\UserException;

class Services
{
    /** @var Customer|null  */
    public $realCustomer;
    /** @var InternetTariff|null  */
    public $realInternetTariff;
    /** @var VoiceTariff|null  */
    public $realVoiceTariff;
    /** @var CustomTariff|null  */
    public $realCustomTariff;

    public const TYPE_INTERNET = 'internet';
    public const TYPE_VOICE = 'voice';
    public const TYPE_CUSTOM = 'custom';

    /**
     * @param int $customerId
     * @param int $internetTariffId
     * @param int $voiceTariffId
     * @param int $customTariffId
     * @throws UserException
     * @throws ApiResponseException
     */
    public function __construct($customerId, $internetTariffId, $voiceTariffId, $customTariffId)
    {
        $this->realCustomer = (new Customer())->findById($customerId);
        $this->realInternetTariff = (new InternetTariff())->findById($internetTariffId);
        $this->realVoiceTariff = (new VoiceTariff())->findById($voiceTariffId);
        $this->realCustomTariff = (new CustomTariff())->findById($customTariffId);

        if ($this->realCustomer === null) {
            throw new UserException('Customer not found');
        }
    }

    /**
     * Create customer services by tariffs ids
     * @return array<mixed>
     */
    public function create()
    {
        if (empty($this->realInternetTariff) && empty($this->realVoiceTariff) && empty($this->realCustomTariff)) {
            $result['internet'] = $result['voice'] = $result['custom'] = null;
            return $result;
        }
        return [
            'internet' => $this->addService(),
            'voice' => $this->addService(self::TYPE_VOICE),
            'custom' => $this->addService(self::TYPE_CUSTOM),
        ];
    }

    /**
     * @param string|int $customer_id
     * @param string $type
     * @return string
     */
    public function servicesApiCall($customer_id, $type = self::TYPE_INTERNET)
    {
        return 'admin/customers/customer/' . $customer_id . '/' . $type . '-services';
    }

    /**
     * Add service by type via API call
     * @param string $type
     * @return null|string|int
     */
    public function addService($type = self::TYPE_INTERNET)
    {
        switch ($type) {
            case self::TYPE_CUSTOM:
                $tariff = $this->realCustomTariff;
                break;
            case self::TYPE_VOICE:
                $tariff = $this->realVoiceTariff;
                break;
            case self::TYPE_INTERNET;
            default;
                $tariff = $this->realInternetTariff;
                break;
        }
        if ($this->realCustomer == null || $tariff == null) {
            return null;
        }

        $params = $this->getServiceParams($tariff, $type);
        if (empty($params)) {
            return null;
        }

        return $this->realCustomer->addService($type, $params);
    }

    /**
     * Params for API call POST create customer service by type
     * @param InternetTariff|VoiceTariff|CustomTariff $tariff
     * @param string $type
     * @return array<mixed>
     */
    private function getServiceParams($tariff, $type = self::TYPE_INTERNET)
    {
        $status = ConfigHelper::get('new_customer_status');
        $params = [
            'status' => ($status && $status === BaseCustomer::STATUS_NEW) ? BaseService::STATUS_STOPPED : BaseService::STATUS_ACTIVE,
            'start_date' => date('Y-m-d'),
            'end_date' => '0000-00-00',
            'quantity' => '1',
        ];

        /** @var Customer $realCustomer */
        $realCustomer = $this->realCustomer;

        switch ($type) {
            case self::TYPE_CUSTOM:
                break;
            case self::TYPE_VOICE:
                $params['phone'] = '';
                break;
            case self::TYPE_INTERNET:
                $params['taking_ipv4'] = '0';
                $params['login'] = $realCustomer->login;
                $params['password'] = $realCustomer->password;
                break;
            default:
                return [];
        }
        $params['customer_id'] = $realCustomer->id;
        $params['tariff_id'] = $tariff->id;
        $params['description'] = $tariff->title;
        $params['unit_price'] = $tariff->price;
        return $params;
    }
}
