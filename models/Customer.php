<?php

namespace app\models;

use splynx\v2\models\customer\BaseCustomer;
use splynx\v2\models\services\BaseCustomService;
use splynx\v2\models\services\BaseInternetService;
use splynx\v2\models\services\BaseVoiceService;

class Customer extends BaseCustomer
{
    /**
     * add service to customer
     * @param string $type
     * @param array<mixed> $params
     * @return int|null
     */
    public function addService($type, $params)
    {
        switch ($type) {
            case Services::TYPE_CUSTOM :
                $service = new BaseCustomService($params);
                break;
            case Services::TYPE_INTERNET :
                $service = new BaseInternetService($params);
                break;
            case Services::TYPE_VOICE :
                $service = new BaseVoiceService($params);
                break;
            default:
                return null;
        }

        if ($service->save()) {
            return $service->id;
        }
        return null;
    }
}
