<?php

namespace app\models;

use app\models\traits\TariffsTrait;
use splynx\v2\models\tariffs\BaseInternetTariff;

/**
 * Class InternetTariff
 * @package app\models
 */
class InternetTariff extends BaseInternetTariff
{
    use TariffsTrait;

    /**
     * Available internet tariffs for self-registration
     * @return InternetTariff[]|null
     */
    public function getAvailableInternetTariffs()
    {
        return $this->findAll($this->getMainConditionForAvailableTariffs(), [
            'available_when_register_by_social_network' => '1',
        ]);
    }
}
