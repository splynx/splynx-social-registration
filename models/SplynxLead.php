<?php

namespace app\models;

use splynx\base\ApiResponseException;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\crm\BaseLead;
use splynx\v2\models\crm\BaseLeadsInfo;
use yii\base\InvalidConfigException;

class SplynxLead extends BaseLead
{
    use SplynxCustomerTrait;

    /**
     * @param Customers $customer
     * @return void
     * @throws InvalidConfigException
     */
    public function createServices($customer)
    {
        $config = ConfigHelper::getParams();

        if (isset($config['admin_notification']) && $config['admin_notification'] && !empty($config['splynx_admin_id'])) {
            RequestSend::adminNotification($config['splynx_admin_id'], $this->id, $config['splynx_url']);
        }
    }

    /**
     * @param string $status
     * @return void
     * @throws ApiResponseException
     */
    public function processStatus($status)
    {
        /** @var BaseLeadsInfo $info */
        $info = (new BaseLeadsInfo())->findById($this->id);
        $info->crm_status = $status;
        $info->save();
    }
}
