<?php

namespace app\models;

use splynx\helpers\ConfigHelper;

class SplynxCustomers
{
    public static function get()
    {
        $register_as = ConfigHelper::get('register_as');

        return $register_as == 'customer' ? new SplynxCustomer() : new SplynxLead();
    }
}
