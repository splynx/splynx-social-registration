<?php

namespace app\models;

use splynx\base\ApiResponseException;
use splynx\helpers\ConfigHelper;
use splynx\models\console_api\config\ConsoleSplynxConfig;
use splynx\v2\base\BaseActiveApi;
use yii\base\InvalidConfigException;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

trait SplynxCustomerTrait
{
    /**
     * @var int|string|null
     */
    public $code;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['code', 'required', 'on' => 'confirm'],
            ['code', 'integer'],
        ]);
    }

    /**
     * @return void
     */
    public function init()
    {
        $this->setIsNewRecord(true);
    }

    /**
     * @param Customers $customer
     * @return array<string, bool|string>
     */
    public function addCustomer($customer)
    {
        $addResult = [
            'result' => false,
            'message' => 'Wrong verification code!',
        ];
        $config = ConfigHelper::getParams();
        if (
            // @phpstan-ignore-next-line
            $config['billing_type'] == $customer::TYPE_PREPAID &&
            // @phpstan-ignore-next-line
            (new ConsoleSplynxConfig())->findOne(
                ['path' => 'settings', 'module' => 'finance', 'key' => 'prepaid_daily_enabled']
            )->value != 1
        ) {
            $billingType = $customer::TYPE_PREPAID_MONTHLY;
        } else {
            $billingType = $customer->billing_type;
        }

        if ($customer != null) {
            $params = [
                'login' => $customer->login,
                'password' => $customer->password,
                'name' => $customer->full_name,
                'email' => $customer->email,
                'phone' => $customer->phone,
                'zip_code' => $customer->zip_code,
                'city' => $customer->city,
                'street_1' => $customer->street,
                'category' => $customer->category,
                'status' => $customer->status,
                'added_by' => $customer->added_by,
                'billing_type' => $billingType,
                'date_add' => $customer->date_add,
                'location_id' => empty($customer->location_id) ? $customer->getLocationId() : $customer->location_id,
                'partner_id' => empty($customer->partner_id) ? $customer->getPartnerId() : $customer->partner_id,
                'additional_attributes' => [
                    'social_id' => $customer->social_id,
                    'self_registration_comment' => $customer->comment,
                ],
            ];

            self::populate($this, $params);
            if ($this->create()) {
                $this->processStatus($customer->status);
                $addResult['result'] = true;
                $addResult['message'] = '';
                self::createServices($customer);
                $customer->delete();
            } else {
                $addResult['message'] = 'Error while adding customer by API!';
            }
        }
        return $addResult;
    }

    /**
     * @param string $status
     * @return void
     */
    public function processStatus($status)
    {
    }

    /**
     * @param Customers $customer
     * @return void
     * @throws ApiResponseException
     * @throws InvalidConfigException
     * @throws UserException
     * @throws BadRequestHttpException
     */
    public function createServices($customer)
    {
        $config = ConfigHelper::getParams();
        $servicesCreator = new Services($this->id, $customer->internet_tariff, $customer->voice_tariff, $customer->custom_tariff);
        $services = $servicesCreator->create();
        $isContract = (isset($config['create_customer_contract']) && $config['create_customer_contract']);

        if ($isContract) {
            Contract::process($this->id, $services);
        }

        if (isset($config['admin_notification']) && $config['admin_notification'] && !empty($config['splynx_admin_id'])) {
            $isService = (!empty($services['internet']) || !empty($services['voice']) || !empty($services['custom']));
            RequestSend::adminNotification($config['splynx_admin_id'], $this->id, $config['splynx_url'], $isService, $isContract);
        }
    }

    /**
     * @return array<int, string>
     */
    public function getAttributesForSendArray()
    {
        $attributes = $this->attributes();
        $attributes = array_filter($attributes, function ($el) {
            return $el != 'code';
        });

        return $attributes;
    }

    /**
     * @param string $login
     * @return SplynxCustomer|SplynxLead|null
     */
    public function getRealCustomersByLogin($login)
    {
        if (empty($login)) {
            return null;
        }
        return $this->findOne(['login' => $login]);
    }

    /**
     * @param string $attribute
     * @param string $value
     * @return BaseActiveApi|null
     */
    public function getRealCustomerByAttribute($attribute, $value)
    {
        if (empty($attribute) || empty($value)) {
            return null;
        }

        if (!($customersReal = $this->findAll([$attribute => ['LIKE', $value]]))) {
            return null;
        }

        foreach ($customersReal as $customer) {
            if ($this->assertValueExists($customer->{$attribute}, $value)) {
                $customer->setIsNewRecord(false);
                return $customer;
            }
        }
        return null;
    }

    /**
     * @param string|int|null $id
     * @return SplynxCustomer|SplynxLead|null
     */
    public function getCustomerBySocialId($id)
    {
        return $this->findOne([], ['social_id' => $id]);
    }

    /**
     * @param string|null $socialId
     * @return bool
     */
    public function setSocialIdForCustomer($socialId)
    {
        $this->additional_attributes['social_id'] = $socialId;
        return $this->save();
    }

    /**
     * Check for isset attriibute in customer
     * @param string $values atttribute get from customer
     * @param string $value value for comparison
     * @return bool
     */
    public static function assertValueExists($values, $value)
    {
        $exists = array_filter(array_map('trim', explode(',', $values)), function ($val) use ($value) {
            return strtolower($val) === strtolower(trim($value));
        });

        return count($exists) > 0;
    }

    /**
     * @param bool $value
     * @return void
     */
    public function setIsNewRecord($value)
    {
        $this->isNewRecord = $value;
    }
}
