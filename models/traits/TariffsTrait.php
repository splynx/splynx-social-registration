<?php

namespace app\models\traits;

use splynx\helpers\ConfigHelper;

/**
 * Trait TariffsTrait
 * @package app\models\traits
 */
trait TariffsTrait
{
    /**
     * @return array<string, string|empty>
     */
    public function getMainConditionForAvailableTariffs()
    {
        if ($splynxVersion = ConfigHelper::getSpynxVersion()) {
            if (version_compare($splynxVersion, '3.1') >= 0) {
                return [
                    'available_for_services' => '1',
                ];
            }
        }
        return [];
    }
}
