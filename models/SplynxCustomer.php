<?php

namespace app\models;

use splynx\v2\models\customer\BaseCustomer;

class SplynxCustomer extends BaseCustomer
{
    use SplynxCustomerTrait;
}
