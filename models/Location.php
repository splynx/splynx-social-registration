<?php

namespace app\models;

use splynx\v2\models\administration\BaseLocation;

class Location extends BaseLocation
{
    /**
     * @return Location[]
     */
    public function getAvailableLocations()
    {
        return $this->findAll([], ['available_in_self_registration' => '1']);
    }
}
