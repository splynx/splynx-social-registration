<?php

namespace app\models;

use app\models\traits\TariffsTrait;
use splynx\v2\models\tariffs\BaseCustomTariff;

/**
 * Class CustomTariff
 * @package app\models
 */
class CustomTariff extends BaseCustomTariff
{
    use TariffsTrait;

    /**
     * @return CustomTariff[]
     */
    public function getAvailableCustomTariffs()
    {
        return $this->findAll($this->getMainConditionForAvailableTariffs(), [
            'available_in_self_registration' => '1',
        ]);
    }
}
