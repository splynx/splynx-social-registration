<?php

namespace app\models;

use Mpdf\Mpdf;
use splynx\helpers\ApiHelper;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\customer\BaseSendDocument;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\web\BadRequestHttpException;

class Contract extends Model
{
    public const CONTRACT_FILE_NAME_PREFIX = 'contract_';

    /**
     * @param int $customerId
     * @param array<mixed> $services
     * @return void
     * @throws InvalidConfigException
     * @throws BadRequestHttpException
     */
    public static function process($customerId, $services)
    {
        $pdf = new Mpdf(); // @phpstan-ignore-line

        $config = ConfigHelper::getParams();

        $templateId = isset($config['contract_pdf_template_id']) ? $config['contract_pdf_template_id'] : 0;
        $template = ApiHelper::getInstance()->get('admin/config/templates/' . $templateId . '-render-' . $customerId);

        $contractText = ($template['result'] && !empty($template['response']['result']))
            ? $template['response']['result'] : 'Wrong contract template id, please contact our administrator';

        $pdf->WriteHTML($contractText); // @phpstan-ignore-line

        $name = self::CONTRACT_FILE_NAME_PREFIX . $customerId . (!empty($services['internet']) ? ('_i' . $services['internet']) : '') .
            (!empty($services['voice']) ? ('_v' . $services['voice']) : '') .
            (!empty($services['custom']) ? ('_c' . $services['custom']) : '');
        $pdfPath = (isset($config['contract_pdf_path']) && is_writable($config['contract_pdf_path'])) ? $config['contract_pdf_path'] : '/tmp/';
        $path = $pdfPath . $name . '.pdf';
        $pdf->Output($path, 'F'); // @phpstan-ignore-line
        if (file_exists($path)) {
            $params = [
                'customer_id' => $customerId,
                'type' => 'uploaded',
                'title' => $name,
                'description' => 'Self-Registration agreement',
                'visible_by_customer' => '1',
            ];
            $customerDocument = new CustomerDocument($params);
            if ($customerDocument->upload($path)) {
                $sendParams = [
                    'type' => 'mail',
                    'id' => $customerDocument->id,
                    'subject' => 'ISP Self-Registration agreement',
                    'document_type' => 'document',
                ];

                if (isset($config['contract_email_template_id']) && !empty($config['contract_email_template_id'])) {
                    $sendParams['template_id'] = $config['contract_email_template_id'];
                } else {
                    $sendParams['message'] = 'Self-Registration agreement ' . $name;
                }
                (new BaseSendDocument($sendParams))->save();
                unlink($path);
            }
        }
    }
}
