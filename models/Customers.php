<?php

namespace app\models;

use splynx\helpers\ConfigHelper;
use splynx\v2\models\customer\BaseCustomer as Customer;
use Yii;
use yii\authclient\widgets\AuthChoice;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class Customers
 * @package app\models
 *
 * @property int $id
 * @property int $partner_id
 * @property int $location_id
 * @property int $added_by_id
 * @property string $date_add
 * @property string $billing_type
 * @property string $added_by
 * @property string $status
 * @property string|null $login
 * @property string $category
 * @property string $password
 * @property string $password_repeat
 * @property string $full_name
 * @property string|null $email
 * @property string|null $phone
 * @property bool $license_agreement
 * @property string $street
 * @property string $zip_code
 * @property string $city
 * @property string $comment
 * @property string $verification_code
 * @property string $social_id
 * @property string $real_added
 * @property int $internet_tariff
 * @property int $voice_tariff
 * @property int $custom_tariff
 */
class Customers extends ActiveRecord
{
    /** @var string */
    public $password_repeat;
    /** @var bool */
    public $license_agreement;
    public const IN_PROCESS = 'in_process';
    public const IN_PROCESS_SOCIAL = 'in_process_social';
    public const ADDED = 'added';

    public const GOOGLE_PREFIX = 'google-';
    public const TWITTER_PREFIX = 'twitter-';
    public const FACEBOOK_PREFIX = 'fb-';

    public const TYPE_RECURRING = 'recurring';
    public const TYPE_PREPAID = 'prepaid';
    public const TYPE_PREPAID_MONTHLY = 'prepaid_monthly';

    public static function tableName()
    {
        return '{{%customers}}';
    }

    /**
     * @return array<int, array<int|string, array<int, string>|int|string>>
     */
    public function rules()
    {
        $requiredFields = ['email', 'full_name'];

        if (ConfigHelper::get('send_verification_code_type') == 'sms') {
            $requiredFields[] = 'phone';
        }
        if (ConfigHelper::get('form_password')) {
            $requiredFields[] = 'password';
            $requiredFields[] = 'password_repeat';
        }

        return [
            [['id',
                'partner_id',
                'location_id',
                'added_by_id',
                'verification_code',
                'internet_tariff',
                'voice_tariff',
                'custom_tariff',], 'integer',],
            [['billing_type',
                'added_by',
                'status',
                'login',
                'category',
                'password_repeat',
                'full_name',
                'email',
                'phone',
                'street',
                'zip_code',
                'city',
                'real_added',
                'social_id',
                'comment',], 'string',],
            ['password', 'string', 'min' => 6],
            ['date_add', 'safe'],
            ['license_agreement', 'boolean', 'trueValue' => 1, 'falseValue' => 1, 'message' => 'You must read and accept the terms in Customer agreement if you want to continue'],
            [$requiredFields, 'required'],
            ['email', 'email'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
            [['login'], 'validateLogin'],
        ];
    }

    /**
     * @param string $attribute
     * @param array<mixed> $params
     * @param mixed $validator
     * @return bool
     */
    public function validateLogin($attribute, $params, $validator)
    {
        if (stripos($this->$attribute, '&') !== false) {
            $this->addError($attribute, Yii::t('app', 'Login must not contain the symbol &'));
            return false;
        }
        return true;
    }

    /**
     * @return string[]
     */
    public function attributeLabels()
    {
        return [
            'internet_tariff' => 'Choose Internet plan',
            'voice_tariff' => 'Choose Voice plan',
            'custom_tariff' => 'Choose Custom plan',
        ];
    }

    /**
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->date_add = date('Y-m-d');
        $this->verification_code = (string)mt_rand(100000, 999999);
        $this->billing_type = $this->getBillingType();
        $this->added_by = 'api';

        $register_as = ConfigHelper::get('register_as');
        if ($register_as == 'customer') {
            $this->status = ConfigHelper::get('new_customer_status');
        } else {
            $this->status = ConfigHelper::get('new_lead_status');
        }

        $this->category = 'person';
        $this->real_added = self::IN_PROCESS;
    }

    /**
     * @param int|string|null $code
     * @return Customers|null
     */
    public static function findCustomerByVerificationCode($code)
    {
        /** @var Customers|null $customers */
        $customers = self::find()->where(['verification_code' => $code])->one();
        return $customers;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return Customer::findById($this->id); //@phpstan-ignore-line
    }

    /**
     * Get billing type
     *
     * @return string
     */
    public function getBillingType()
    {
        if ($type = ConfigHelper::get('billing_type')) {
            if ($type == self::TYPE_PREPAID || $type == self::TYPE_RECURRING || $type == self::TYPE_PREPAID_MONTHLY) {
                return $type;
            }
        }
        return self::TYPE_RECURRING;
    }

    /**
     * Available Internet Tariffs for select in self-registration by partner
     * @param null|int|string $partner
     * @return array<int|empty, string|empty>
     */
    public function getAvailableTariffsList($partner = null)
    {
        $internetTariffs = (new InternetTariff())->getAvailableInternetTariffs();
        if (!empty($internetTariffs)) {
            $internetTariffs = $this->filterTariffsByPartner($internetTariffs, $partner);
        }
        return !empty($internetTariffs) ? ArrayHelper::map($internetTariffs, 'id', 'title') : [];
    }

    /**
     * Available partners for select in self-registration
     * @return array<int|empty, string|empty>
     */
    public function getAvailablePartnersList()
    {
        if (!ConfigHelper::get('form_partner')) {
            return [];
        }
        $partners = (new Partner())->getAvailablePartners();

        return !empty($partners) ? ArrayHelper::map($partners, 'id', 'name') : [];
    }

    /**
     * Available locations for select in self-registration
     * @return array<int|empty, string|empty>
     */
    public function getAvailableLocationsList()
    {
        $locations = (new Location())->getAvailableLocations();

        return !empty($locations) ? ArrayHelper::map($locations, 'id', 'name') : [];
    }

    /**
     * Available Voice Tariffs for select in self-registration by partner
     * @param null|int|string $partner
     * @return array<int|empty, string|empty>
     */
    public function getAvailableVoiceTariffsList($partner = null)
    {
        $voiceTariffs = (new VoiceTariff())->getAvailableVoiceTariffs();
        if (!empty($voiceTariffs)) {
            $voiceTariffs = $this->filterTariffsByPartner($voiceTariffs, $partner);
        }
        return !empty($voiceTariffs) ? ArrayHelper::map($voiceTariffs, 'id', 'title') : [];
    }

    /**
     * Available Custom Tariffs for select in self-registration by partner
     * @param null|int|string $partner
     * @return array<int|empty, string|empty>
     */
    public function getAvailableCustomTariffsList($partner = null)
    {
        $tariffs = (new CustomTariff())->getAvailableCustomTariffs();
        if (!empty($tariffs)) {
            $tariffs = $this->filterTariffsByPartner($tariffs, $partner);
        }
        return !empty($tariffs) ? ArrayHelper::map($tariffs, 'id', 'title') : [];
    }

    /**
     * Check included social registration (work by component yiisoft authclient)
     *
     * @return bool
     */
    public function checkSocialRegistration()
    {
        $model = new AuthChoice();
        $clients = $model->getClients();
        if (empty($clients)) {
            return false;
        }
        return true;
    }

    /**
     * Get partner ID
     * @return int
     */
    public function getPartnerId()
    {
        if ($partner_id = ConfigHelper::get('partner_id')) {
            return (int)$partner_id;
        }
        return 1;
    }

    /**
     * Get location ID
     * @return int
     */
    public function getLocationId()
    {
        if ($location_id = ConfigHelper::get('location_id')) {
            return (int)$location_id;
        }
        return 1;
    }

    /**
     * Licence label for web checkbox
     * @return string
     */
    public function licenseLabel()
    {
        return 'I accept the terms in the <a href="javascript:void(0);" onclick="window.open(\'/register/license\', \'_blank\');">Customer agreement</a>';
    }

    /**
     * Method return only tariffs available for $partner or for partners by config
     * @param array<mixed> $tariffs
     * @param null|string|int $partner
     * @return array<mixed>
     */
    private function filterTariffsByPartner($tariffs, $partner)
    {
        if (empty($partner) && ConfigHelper::get('form_partner') && (!empty(($partners = $this->getAvailablePartnersList())))) {
            foreach ($tariffs as $key => $tariff) {
                $allow = false;
                foreach ($tariff->partners_ids as $id) {
                    if (isset($partners[$id])) {
                        $allow = true;
                        break;
                    }
                }
                if (!$allow) {
                    unset($tariffs[$key]);
                }
            }
        } elseif (!empty($partner)) {
            foreach ($tariffs as $key => $tariff) {
                if (!in_array($partner, $tariff->partners_ids)) {
                    unset($tariffs[$key]);
                }
            }
        }
        return $tariffs;
    }

    /**
     * @return string|null
     */
    public function checkCustomerExists()
    {
        $message = null;
        if ((SplynxCustomers::get())->getRealCustomersByLogin($this->login)) {
            $message = "Customer with Login: $this->login  already exist. Choose other.";
            $this->login = null;
        } elseif ((SplynxCustomers::get())->getRealCustomerByAttribute('email', $this->email)) {
            $message = "Customer with Email: $this->email  already exist. Choose other.";
            $this->email = null;
        } elseif ((SplynxCustomers::get())->getRealCustomerByAttribute('phone', $this->phone)) {
            $message = "Customer with Phone number: $this->phone  already exist. Choose other.";
            $this->phone = null;
        }
        return $message;
    }
}
